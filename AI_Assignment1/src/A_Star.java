import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class A_Star {
	private List<Node> visited = new ArrayList<Node>();
	private final int mapWidth = 101;
	private final int mapHeight = 101;

	public int manhattanDist(Node src, Node dest) {
		return ((Math.abs(dest.getX() - src.getX()) + (Math.abs(dest.getY()
				- src.getY()))));
	}

	public Node initiateNode(Node n, String s) {
		if (s.equalsIgnoreCase("start")) {
			n.setStart(true);
			n.setG_distFromStart(0);
		} else if (s.equals("goal")) {
			n.setGoal(true);
		}
		return n;
	}

	public List<Node> getNeighbors(Node n, Node[][] map) {
		List<Node> neighbors = new ArrayList<Node>();
		int x = n.getX();
		int y = n.getY();
		if (!((x - 1) < 0)) {
			n.setLeft(map[x - 1][y]);
			neighbors.add(n.getLeft());
		}
		if (!((y - 1) < 0)) {
			n.setTop(map[x][y - 1]);
			neighbors.add(n.getTop());
		}
		if (!((x + 1) > (mapWidth - 1))) {
			n.setRight(map[x + 1][y]);
			neighbors.add(n.getRight());
		}
		if (!((y + 1) > (mapHeight - 1))) {
			n.setBottom(map[x][y + 1]);
			neighbors.add(n.getBottom());
		}
		return neighbors;
	}

	public Node[][] initializeHeuristicDistance(Node[][] map, Node startNode,
			Node goalNode) {
		for (int i = 0; i < mapWidth; i++) {
			for (int j = 0; j < mapHeight; j++) {
				Node temp = map[i][j];
				// temp.setG_distFromStart(manhattanDist(startNode, temp));
				temp.setH_heurDistFromGoal(manhattanDist(temp, goalNode));
				map[i][j] = temp;
			}
		}
		return map;
	}

	public boolean constructPath(Node[][] map, Node startNode, Node goalNode)
			throws AStarException {
		Node currentNode = new Node(-1, -1);
		// PathNodeList pathSet = new PathNodeList();
		Comparator<Node> comparator = new NodeComparatorLargerG();
		PriorityQueue<Node> pathSet = new PriorityQueue<Node>(101, comparator);
		pathSet.add(startNode);
		// pathSet.insert(startNode);
		List<Node> neighbors = new ArrayList<Node>();
		while (pathSet.size() > 0) {
			/*
			 * for(int i=0;i<pathSet.size();i++){ Node node = pathSet.fetch(i);
			 * System
			 * .out.print(node.getX()+","+node.getY()+":"+node.getG_distFromStart
			 * ()+node.getH_heurDistFromGoal()+"    "); } for(Node
			 * node:pathSet){
			 * System.out.print(node.getX()+","+node.getY()+":"+node
			 * .getG_distFromStart()+node.getH_heurDistFromGoal()+"    "); }
			 */
			// currentNode = pathSet.getMin();
			currentNode = pathSet.remove();
			System.out.println("\nMin : " + currentNode.getX() + ","
					+ currentNode.getY() + ":"
					+ currentNode.getG_distFromStart()
					+ currentNode.getH_heurDistFromGoal());
			// pathSet.remove(currentNode);
			if (currentNode.equals(goalNode)) {
				return true;
			}
			visited.add(currentNode);
			neighbors = getNeighbors(currentNode, map);
			int g_dist_neighbor = 0;
			for (Node x : neighbors) {
				if (visited.contains(x) || x.isObstacle()) {
					continue;
				} else {
					g_dist_neighbor = currentNode.getG_distFromStart() + 1;
					if (!pathSet.contains(x)
							|| g_dist_neighbor < x.getG_distFromStart()) {
						x.setG_distFromStart(g_dist_neighbor);
						x.setParentNode(currentNode);
						x.setH_heurDistFromGoal(manhattanDist(x, goalNode));
						if (pathSet.contains(x)) {
							pathSet.remove(x);
						}
						pathSet.add(x);
					}
				}
			}
		}
		throw new AStarException("A Path could not be found");
	}

	public List<Node> displayPath(Node startNode, Node goalNode) {
		Node n = goalNode;
		List<Node> path = new ArrayList<Node>();
		List<Node> drawPath = new ArrayList<Node>();
		path.add(n);
		while (!n.equals(startNode)) {
			n = n.getParentNode();
			path.add(n);
			if (n != startNode && n != goalNode) {
				drawPath.add(n);
			}
		}
		for (int i = path.size() - 1; i > 0; i--) {
			System.out.print("(" + (path.get(i)).getX() + ","
					+ (path.get(i)).getY() + ")-->");
		}
		System.out.print("(" + (path.get(0)).getX() + ","
				+ (path.get(0)).getY() + ")");
		return drawPath;
	}

	public Node generateRandomNode(int gridWidth, int gridHeight) {
		int x_coord = (int) Math.floor(Math.random() * gridWidth);
		int y_coord = (int) Math.floor(Math.random() * gridHeight);
		Node n = new Node(x_coord, y_coord);
		return n;
	}

	public static void main(String args[]) {
		MazeCreator_DFS mazeCreator = new MazeCreator_DFS();
		A_Star aStar = new A_Star();
		Node[][] gridMap = mazeCreator.initializeMap();
		gridMap = mazeCreator.createMap(gridMap);
		boolean startFound = false;
		boolean goalFound = false;
		Node tempStartNode = new Node(-1, -1);
		Node tempGoalNode = new Node(-1, -1);
		Node startNode = new Node(-1, -1);
		Node goalNode = new Node(-1, -1);
		while (!startFound) {
			tempStartNode = aStar.generateRandomNode(aStar.mapWidth,
					aStar.mapHeight);
			startNode = gridMap[tempStartNode.getX()][tempStartNode.getY()];
			if (startNode.isObstacle() || startNode.isGoal()) {
				startFound = false;
			} else {
				startFound = true;
			}
		}
		startNode = aStar.initiateNode(startNode, "start");
		startNode.setH_heurDistFromGoal(aStar
				.manhattanDist(startNode, goalNode));
		while (!goalFound) {
			tempGoalNode = mazeCreator.generateRandomNode(aStar.mapWidth,
					aStar.mapHeight);
			goalNode = gridMap[tempGoalNode.getX()][tempGoalNode.getY()];
			if (goalNode.isObstacle() || goalNode.isStart()) {
				goalFound = false;
			} else {
				goalFound = true;
			}
		}
		gridMap = aStar.initializeHeuristicDistance(gridMap, startNode,
				goalNode);
		goalNode = aStar.initiateNode(goalNode, "goal");
		System.out.println("Start Node..." + startNode.getX() + ","
				+ startNode.getY());
		System.out.println("Stop Node..." + goalNode.getX() + ","
				+ goalNode.getY());
		boolean pathExists;
		List<Node> drawPath = new ArrayList<Node>();
		try {
			pathExists = aStar.constructPath(gridMap, startNode, goalNode);
			/*
			 * if (path.size() == 1) { path.add(goalNode); } Iterator<Node> iter
			 * = path.iterator(); while (iter.hasNext()) { Node x = iter.next();
			 * if (iter.hasNext()) { System.out.print("(" + x.getX() + "," +
			 * x.getY() + ")-->"); } else { System.out.print("(" + x.getX() +
			 * "," + x.getY() + ")"); } }
			 */
			if (pathExists) {
				drawPath = aStar.displayPath(startNode, goalNode);
			}
		} catch (AStarException a) {
			System.out.println(a.getMessage());
		}
		System.out.println();
		DrawMaze drawMaze = new DrawMaze();
		drawMaze.startNode(startNode.getX(), startNode.getY());
		drawMaze.goalNode(goalNode.getX(), goalNode.getY());
		JFrame f = new JFrame("Maze");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		for (int i = 0; i < aStar.mapWidth; i++) {
			for (int j = 0; j < aStar.mapHeight; j++) {
				/*
				 * System.out.print((gridMap[i][j]).getX() + "," +
				 * (gridMap[i][j]).getY() + (gridMap[i][j]).isObstacle() +
				 * "     ");
				 */
				if (gridMap[i][j].isObstacle()) {
					drawMaze.fillCell(i, j);
				}
			}
			// System.out.println();
		}
		for (Node pathNode : drawPath) {
			drawMaze.drawPath(pathNode.getX(), pathNode.getY());
		}
		JPanel container = new JPanel();
		JScrollPane scrollPane = new JScrollPane(drawMaze);
		scrollPane.setPreferredSize(new Dimension(1010, 650));
		container.add(scrollPane, BorderLayout.CENTER);
		f.add(container);
		// f.add(drawMaze);
		f.pack();
		f.setVisible(true);

		// pathSet.add(startNode);
	}
}
