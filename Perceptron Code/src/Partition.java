import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Partition {
	public static void main(String args[]) {
		String line;
		String[] splitTxt = null;
		String x1_cord = null;
		String x2_cord = null;
		List<Double> x1 = new ArrayList<Double>();
		List<Double> x2 = new ArrayList<Double>();
		List<Double> exp_class = new ArrayList<Double>();
		FileReader fr = null;
		BufferedReader br = null;
		String filePath = "Perceptron_data.txt";
		try {
			fr = new FileReader(filePath);
			br = new BufferedReader(fr);
			while (true) {
				line = br.readLine();
				if (line == null)
					break;
				line = line.trim();
				if (line.length() == 0)
					continue;
				splitTxt = line.split(",");
				x1_cord = splitTxt[0].substring(1);
				x2_cord = splitTxt[1].substring(0, splitTxt[1].length() - 1);
				if (x1_cord.equalsIgnoreCase("x1")) {
					continue;
				}
				x1.add(Double.parseDouble(x1_cord));
				x2.add(Double.parseDouble(x2_cord));
				exp_class.add(Double.parseDouble(splitTxt[2]));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			try {
				br.close();
				fr.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
