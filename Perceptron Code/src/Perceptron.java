import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Perceptron {
	public static void main(String args[]) {
		String line;
		String[] splitTxt = null;
		String x1_cord = null;
		String x2_cord = null;
		List<Double> x1 = new ArrayList<Double>();
		List<Double> x2 = new ArrayList<Double>();
		List<Double> exp_class = new ArrayList<Double>();
		FileReader fr = null;
		BufferedReader br = null;
		String filePath = "Perceptron_data.txt";
		try {
			fr = new FileReader(filePath);
			br = new BufferedReader(fr);
			while (true) {
				line = br.readLine();
				if (line == null)
					break;
				line = line.trim();
				if (line.length() == 0)
					continue;
				splitTxt = line.split(",");
				x1_cord = splitTxt[0].substring(1);
				x2_cord = splitTxt[1].substring(0, splitTxt[1].length() - 1);
				if (x1_cord.equalsIgnoreCase("x1")) {
					continue;
				}
				x1.add(Double.parseDouble(x1_cord));
				x2.add(Double.parseDouble(x2_cord));
				exp_class.add(Double.parseDouble(splitTxt[2]));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			try {
				br.close();
				fr.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		int numInstance = x1.size();
		final int THETA = 0;
		int output;
		boolean misClassed = true;
		double weights[] = new double[3];
		weights[0] = 0.2;
		weights[1] = 1;
		weights[2] = -1;
		double fx;
		int iter = 0;
		System.out.println("Points : ");
		for (int i = 0; i < numInstance; i++) {
			System.out.println("(" + x1.get(i) + "," + x2.get(i) + ") ; Expected Class : "+exp_class.get(i));
		}
		
		System.out.println("\n\nCalculations : ");
		do {
			System.out.println("Iteration "+iter+" : ");
			misClassed = false;
			for (int i = 0; i < numInstance; i++) {
				fx = weights[0] + weights[1] * x1.get(i) + weights[2]
						* x2.get(i);
				output = (fx >= THETA) ? 1 : -1;
				System.out.print("(" + x1.get(i) + "," + x2.get(i) + ")"+" ; Calculated Value : "+fx+" ; Class : "+output);
				if (output != exp_class.get(i)) {
					System.out.println("; Expected Class : "+exp_class.get(i)+" -> Misclassified");
					weights[0] = weights[0]+exp_class.get(i);
					weights[1] = weights[1]+exp_class.get(i)*x1.get(i);
					weights[2] = weights[2]+exp_class.get(i)*x2.get(i);
					misClassed = true;
				}else{
					System.out.println(" -> Classified Correctly");
				}
			}
			iter++;
			System.out.println(weights[0] + "*1 + " + weights[1] + "*x1 + "
					+ weights[2] + "*x2 = 0 ");
		/*	System.out.println("Iteration : " + iter + " : RMSE = "
					+ Math.sqrt(globalError / numInstance));*/
		} while (misClassed);
		System.out.println("\nDecision boundary equation : \n");
		System.out.println(weights[0] + "*1 + " + weights[1] + "*x1 + "
				+ weights[2] + "*x2 = 0 ");

	}
}
