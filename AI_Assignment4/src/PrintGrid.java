public class PrintGrid {

	public static void main(String args[]) {
		PrintGrid pr = new PrintGrid();
		double[] util = new double[] { 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8,
				0.9, 0.10, 0.11 };
		pr.printGrid(3, 4, util);
	}

	public void printGrid(int rows, int cols, double[] utility) {
		double[][] states = new double[rows][cols];
		int r = rows - 1, c = 0;
		for (int i = 0; i < utility.length; i++) {

			if (c == 4) {
				r--;
				c = 0;
			}
			if (r == 1 && c == 1) {
				c++;
			}
			states[r][c] = utility[i];
			c++;
		}
		/*for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				System.out.print(states[i][j]+"    ");
			}
			System.out.println();
		}*/
		
		for (int i = 0; i < cols; i++) {
			System.out.print(" --------------- ");
		}
		System.out.println();

		for (int i = 0; i < rows; i++) {
			System.out.print("|");
			for (int j = 0; j < cols; j++) {
				if (i == 1 && j == 1) {
					System.out.printf("      %s      ", "Wall");
				} else if (i == 0 && j == 3) {
					System.out.printf("      %s      ", "Exit");
				} else if (i == 1 && j == 3) {
					System.out.printf("      %s      ", "Pit ");
				} else {
					System.out.printf("  %+10.8f   ", states[i][j]);
				}
				System.out.print("|");
			}
			System.out.println();
			for (int k = 0; k < cols; k++) {
				System.out.print(" --------------- ");
			}
			System.out.println();
		}

	}
}
