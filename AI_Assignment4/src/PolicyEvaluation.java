/**
 * This is the template of a class that evaluates a given policy,
 * i.e., computes the utility of each state when actions are chosen
 * according to it.  The utility is returned in the public
 * <tt>utility</tt> field.  You need to fill in the constructor.  You
 * may wish to add other fields with other useful information that you
 * want this class to return (for instance, number of iterations
 * before convergence).  You also may add other constructors or
 * methods, provided that these are in addition to the one given below
 * (which is the one that will be automatically tested).  In
 * particular, your code must work properly when run with the
 * <tt>main</tt> provided in <tt>RunCatMouse.java</tt>.
 */
public class PolicyEvaluation {

    /** the computed utility of each state under the given policy */
    public double utility[];
    private int maxNumOfStates;
    private double deltaFactor;

    /**
     * The constructor for this class.  Computes the utility of policy
     * <tt>pi</tt> for the given <tt>mdp</tt> with given
     * <tt>discount</tt> factor, and stores the answer in
     * <tt>utility</tt>.
     */
    public PolicyEvaluation(Mdp mdp, double discount, int pi[]) {

    	this.maxNumOfStates = mdp.numStates;
    	this.utility = new double[this.maxNumOfStates];
    	this.deltaFactor = Math.pow(10,-8) * (1 - discount) / discount;
    	double oldUtility[] = new double[this.maxNumOfStates];
    	while (true){
    		// setting maximum difference = 0 
    		double maxDifference = 0;
    		// Updating the old utility vector (utility vector from previous iteration) using the current utility calculated
    		for (int currState = 0; currState < this.maxNumOfStates; currState++){
    			oldUtility[currState] = this.utility[currState]; 
    		}
    		for (int currState = 0; currState < this.maxNumOfStates; currState++){	    		
	    		this.utility[currState] = PolicyEvaluationIteration(mdp, discount, pi[currState],currState,oldUtility);
	    		double difference = Math.abs(this.utility[currState] - oldUtility[currState]);
	    		if (difference > maxDifference){
	    			maxDifference = difference;
	    		}	    		
	    	}
	    	
	    	if (this.deltaFactor > maxDifference){
	      		break;
	    	}
    	}
    }


    /**
     * This function evaluates the utility for 1 iteration
     * @param mdp
     * @param discount
     * @param currAction
     * @param currState
     * @param utility
     * @return
     */
    private double PolicyEvaluationIteration(Mdp mdp, double discount, int currAction,int currState,double[] utility)
    {
    	double utilitySum =0.0;
		int maxNumOfDestStates = mdp.nextState[currState][currAction].length;
		
		for(int currDestState =0;currDestState<maxNumOfDestStates;currDestState++)
		{
			int currDestStateIndex = mdp.nextState[currState][currAction][currDestState];
			utilitySum = utilitySum + mdp.transProb[currState][currAction][currDestState] * 
					(mdp.reward[currState]+discount*utility[currDestStateIndex]);
		}
    	
		return utilitySum;
    }
}
