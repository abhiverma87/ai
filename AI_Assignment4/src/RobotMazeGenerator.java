import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RobotMazeGenerator {
	private double[][] grid_rewards;
	private int numStates;
	private int numActions;
	private String startStateName;

	public static void main(String args[]) {
		RobotMazeGenerator rmg = new RobotMazeGenerator();
		int rows = 3;
		int cols = 4;
		rmg.grid_rewards = new double[rows][cols];
		rmg.numStates = rows * cols;
		rmg.numActions = 4;
		/*String end_state = "3:4";
		String pit = "2:4";
		List<String> noTransition = new ArrayList<String>();
		noTransition.add(end_state);
		noTransition.add(pit);*/

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				if (i == 1 && j == 1) {
					rmg.grid_rewards[i][j] = Double.NEGATIVE_INFINITY;
					continue;
				}
				if (i == 0 && j == cols - 1) {
					rmg.grid_rewards[i][j] = 1;
				} else if (i == 1 && j == cols - 1) {
					rmg.grid_rewards[i][j] = -1;
				} else {
					rmg.grid_rewards[i][j] = -0.03;
				}
			}
		}
		Writer writer = null;
		try {
			writer = new BufferedWriter(
					new OutputStreamWriter(
							new FileOutputStream(
									"HPMdp.txt"),
							"utf-8"));
			rmg.startStateName = "1:1";
			// creating entry for start state
			writer.write(rmg.startStateName + "\n\n");
			List<String> stateRewards = new ArrayList<String>();
			// creating entry for rewards for states
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					if (rmg.grid_rewards[i][j] != Double.NEGATIVE_INFINITY) {
						stateRewards.add((rows - i) + ":" + (j + 1) + " "
								+ rmg.grid_rewards[i][j] + "\n");
					}
				}
			}
			Collections.sort(stateRewards);
			for(String tempStr:stateRewards){
				writer.write(tempStr);
			}
			writer.write("\n");
			Map<Integer, String> direction_map = new HashMap<Integer, String>();
			direction_map.put(0, "W");
			direction_map.put(1, "N");
			direction_map.put(2, "E");
			direction_map.put(3, "S");
			// creating entries for all possible actions from each state
			String init_state = null;
			String[] final_state = null;
			double[] prob_fstate = null;
			double prob_init_state;
			StringBuilder str;
			List<String> states = new ArrayList<String>();
			int row, col;
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					if (rmg.grid_rewards[i][j] == Double.NEGATIVE_INFINITY)
						continue;
					prob_init_state = 0;
					row = rows - i;
					col = j + 1;
					init_state = row + ":" + col;
					for (int k = 0; k < direction_map.size(); k++) {
						final_state = new String[3];
						prob_fstate = new double[3];
						prob_init_state = 0;
						if (k == 0) {
							if (j - 1 < 0
									|| rmg.grid_rewards[i][j - 1] == Double.NEGATIVE_INFINITY) {
								final_state[0] = "wall";
								prob_fstate[0] = 0.8;
							} else {
								final_state[0] = row + ":" + (col - 1);
								prob_fstate[0] = 0.8;
							}
							if (i - 1 < 0
									|| rmg.grid_rewards[i - 1][j] == Double.NEGATIVE_INFINITY) {
								final_state[1] = "wall";
								prob_fstate[1] = 0.1;
							} else {
								final_state[1] = (row + 1) + ":" + col;
								prob_fstate[1] = 0.1;
							}
							if (i + 1 >= rows
									|| rmg.grid_rewards[i + 1][j] == Double.NEGATIVE_INFINITY) {
								final_state[2] = "wall";
								prob_fstate[2] = 0.1;
							} else {
								final_state[2] = (row - 1) + ":" + col;
								prob_fstate[2] = 0.1;
							}

						} else if (k == 1) {
							if (j - 1 < 0
									|| rmg.grid_rewards[i][j - 1] == Double.NEGATIVE_INFINITY) {
								final_state[0] = "wall";
								prob_fstate[0] = 0.1;
							} else {
								final_state[0] = row + ":" + (col - 1);
								prob_fstate[0] = 0.1;
							}
							if (i - 1 < 0
									|| rmg.grid_rewards[i - 1][j] == Double.NEGATIVE_INFINITY) {
								final_state[1] = "wall";
								prob_fstate[1] = 0.8;
							} else {
								final_state[1] = (row + 1) + ":" + col;
								prob_fstate[1] = 0.8;
							}
							if (j + 1 >= cols
									|| rmg.grid_rewards[i][j + 1] == Double.NEGATIVE_INFINITY) {
								final_state[2] = "wall";
								prob_fstate[2] = 0.1;
							} else {
								final_state[2] = row + ":" + (col + 1);
								prob_fstate[2] = 0.1;
							}
						} else if (k == 2) {
							if (i + 1 >= rows
									|| rmg.grid_rewards[i + 1][j] == Double.NEGATIVE_INFINITY) {
								final_state[0] = "wall";
								prob_fstate[0] = 0.1;
							} else {
								final_state[0] = (row - 1) + ":" + col;
								prob_fstate[0] = 0.1;
							}
							if (i - 1 < 0
									|| rmg.grid_rewards[i - 1][j] == Double.NEGATIVE_INFINITY) {
								final_state[1] = "wall";
								prob_fstate[1] = 0.1;
							} else {
								final_state[1] = (row + 1) + ":" + col;
								prob_fstate[1] = 0.1;
							}
							if (j + 1 >= cols
									|| rmg.grid_rewards[i][j + 1] == Double.NEGATIVE_INFINITY) {
								final_state[2] = "wall";
								prob_fstate[2] = 0.8;
							} else {
								final_state[2] = row + ":" + (col + 1);
								prob_fstate[2] = 0.8;
							}
						} else if (k == 3) {
							if (j - 1 < 0
									|| rmg.grid_rewards[i][j - 1] == Double.NEGATIVE_INFINITY) {
								final_state[0] = "wall";
								prob_fstate[0] = 0.1;
							} else {
								final_state[0] = row + ":" + (col - 1);
								prob_fstate[0] = 0.1;
							}
							if (i + 1 >= rows
									|| rmg.grid_rewards[i + 1][j] == Double.NEGATIVE_INFINITY) {
								final_state[1] = "wall";
								prob_fstate[1] = 0.8;
							} else {
								final_state[1] = (row - 1) + ":" + col;
								prob_fstate[1] = 0.8;
							}
							if (j + 1 >= cols
									|| rmg.grid_rewards[i][j + 1] == Double.NEGATIVE_INFINITY) {
								final_state[2] = "wall";
								prob_fstate[2] = 0.1;
							} else {
								final_state[2] = row + ":" + (col + 1);
								prob_fstate[2] = 0.1;
							}
						}
						str = new StringBuilder(init_state + " " + direction_map.get(k)
								+ " ");
						for (int l = 0; l < 3; l++) {
							if (final_state[l].equalsIgnoreCase("wall")) {
								prob_init_state += prob_fstate[l];
							} else {
								str.append(final_state[l] + " "
										+ prob_fstate[l] + " ");
							}
						}
						if (prob_init_state > 0) {
							str.append(init_state + " " + prob_init_state);
						}
						str.append("\n");
						states.add(str.toString());
					}

				}
			}
			Collections.sort(states);
			for(String tempStr:states){
				//if(!noTransition.contains(tempStr.split(" ")[0]))
				writer.write(tempStr);
			}
		} catch (IOException ex) {
			// report
		} finally {
			try {
				writer.close();
			} catch (Exception ex) {
			}
		}

	}

}
