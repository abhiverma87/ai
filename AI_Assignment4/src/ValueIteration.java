/**
 * This is the template of a class that should run value iteration on a given
 * MDP to compute the optimal policy which is returned in the public
 * <tt>policy</tt> field. The computed optimal utility is also returned in the
 * public <tt>utility</tt> field. You need to fill in the constructor. You may
 * wish to add other fields with other useful information that you want this
 * class to return (for instance, number of iterations before convergence). You
 * also may add other constructors or methods, provided that these are in
 * addition to the one given below (which is the one that will be automatically
 * tested). In particular, your code must work properly when run with the
 * <tt>main</tt> provided in <tt>RunCatMouse.java</tt>.
 */
public class ValueIteration {

	/** the computed optimal policy for the given MDP **/
	public int policy[];

	/** the computed optimal utility for the given MDP **/
	public double utility[];

	/**
	 * The constructor for this class. Computes the optimal policy for the given
	 * <tt>mdp</tt> with given <tt>discount</tt> factor, and stores the answer
	 * in <tt>policy</tt>. Also stores the optimal utility in <tt>utility</tt>.
	 */
	public ValueIteration(Mdp mdp, double discount) {

		// your code here
		System.out.println("Num States : " + mdp.numStates);
		System.out.println("Num Actions : " + mdp.numActions);
		System.out.println("Discount : " + discount);
		int numStates = mdp.numStates;
		this.utility = new double[numStates];
		// Initializing the policy array
		this.policy = new int[numStates];
		double delta = Math.pow(10, -8) * (1 - discount) / discount; /*TODO find out about error factor*/
		double difference;
		double maxDifference;
		double tempUtility[] = new double[numStates];
		boolean continueLoop = true;
		double maxUtil;
		double stateUtil;
		int destinationState;
		int optimalAction;
		int numActions = mdp.numActions;
		while (continueLoop) {
			maxDifference = Double.NEGATIVE_INFINITY;
			for (int i = 0; i < numStates; i++) {
				this.utility[i] = tempUtility[i];
			}
			// Updating the grid space for optimal policies
			for (int i = 0; i < numStates; i++) {
				optimalAction = -1;
				maxUtil = Double.NEGATIVE_INFINITY;
				for (int j = 0; j < numActions; j++) {
					stateUtil = 0.0;
					for (int k = 0; k < mdp.nextState[i][j].length; k++) {
						destinationState = mdp.nextState[i][j][k];
						stateUtil += (mdp.transProb[i][j][k])
								* (this.utility[destinationState]);
					}
					/*System.out.print(this.direction_map.get(i) + "," + stateUtil
							+ "   ");*/
					if (stateUtil > maxUtil) {
						maxUtil = stateUtil;
						optimalAction = j;
					}
				}
				this.policy[i] = optimalAction;
				tempUtility[i] = mdp.reward[i] + (discount)
						* maxUtil;
				difference = Math.abs(this.utility[i] - tempUtility[i]);
				if (difference > maxDifference) {
					maxDifference = difference;
				}
			}
			if (delta > maxDifference) {
				continueLoop = false;
			}
		}
	}
}
