import java.util.Random;
import java.util.Scanner;

/**
 * This is the template of a class that should run policy iteration on
 * a given MDP to compute the optimal policy which is returned in the
 * public <tt>policy</tt> field.  You need to fill in the constructor.
 * You may wish to add other fields with other useful information that
 * you want this class to return (for instance, number of iterations
 * before convergence).  You also may add other constructors or
 * methods, provided that these are in addition to the one given below
 * (which is the one that will be automatically tested).  In
 * particular, your code must work properly when run with the
 * <tt>main</tt> provided in <tt>RunCatMouse.java</tt>.
 */
public class RobotMazePolicyIteration {

    /** the computed optimal policy for the given MDP **/
    public int policy[];
    //private int bestAction;
    private int maxNumOfActions,maxNumOfStates;

    /**
     * The constructor for this class.  Computes the optimal policy
     * for the given <tt>mdp</tt> with given <tt>discount</tt> factor,
     * and stores the answer in <tt>policy</tt>.
     */
    public RobotMazePolicyIteration(Mdp mdp, double discount) {
    	
    	this.maxNumOfActions = mdp.numActions;
    	this.maxNumOfStates = mdp.numStates;
    	
    	this.policy = initRandomPolicy();
    	boolean loopUntill = true;
    	PrintGrid pr=new PrintGrid();
    	Scanner scan = new Scanner(System.in);
    	System.out.println("\n\nRunning Policy Iteration : ");
		int count=0;
    	while(loopUntill)
    	{
    		System.out.println("Iteration No. "+count+" : ");
			count++;
    		PolicyEvaluation currPolicyEval = new PolicyEvaluation (mdp, discount, this.policy);
    		double[] policyEvalUtility = currPolicyEval.utility;
    		pr.printGrid(3, 4, policyEvalUtility);
    		System.out.println("Press return key to continue");
			scan.nextLine();
    		loopUntill = checkPolicyUnchanged(mdp, discount, policyEvalUtility);
    	}
    	
    }
    
    /**
     * This function initilizes the policy for policyiteration algorithm
     * @return
     */
    private int[] initRandomPolicy()
    {
    	int[] randPolicy = new int[this.maxNumOfStates];   	
    	Random rm = new Random();
    	for(int currState = 0; currState < this.maxNumOfStates ;currState++)
    	{
    		randPolicy[currState] = Math.abs(rm.nextInt()) % this.maxNumOfActions;
    	}
    	return randPolicy;
    }
    
    
    /**
     * This function checks if algorithm needs to iterate once more for finding the optimum policy
     * @param mdp
     * @param discount
     * @param policyEvalUtility
     * @return
     */
    private boolean checkPolicyUnchanged(Mdp mdp, double discount,double[] policyEvalUtility)
    {
    	boolean actionChanged = false;
    	
    	for(int currState = 0;currState < this.maxNumOfStates;currState++)
    	{
    		int bestAction = calculateBestUtility(mdp, discount,policyEvalUtility,currState);

    		if(bestAction != this.policy[currState])
    		{
    			actionChanged = true;
    			this.policy[currState]= bestAction;
    		}

    	}
    	
		return actionChanged;
    }
    
    /**
     * This function finds the best action that can be taken from the policy evaluated
     * @param mdp
     * @param discount
     * @param policyEvalUtility
     * @param currState
     * @return
     */
    private int calculateBestUtility(Mdp mdp, double discount,double[] policyEvalUtility,int currState)
    {
    	//init bestUtility and action
    	double bestUtility = Double.NEGATIVE_INFINITY;
    	int bestAction = 0;
    	
    	for(int currAction=0;currAction < this.maxNumOfActions;currAction++)
    	{
    		double utilitySum =0;
    		int maxNumOfDestStates = mdp.nextState[currState][currAction].length;
    		
    		for(int currDestState =0;currDestState<maxNumOfDestStates;currDestState++)
    		{
    			int currDestStateIndex = mdp.nextState[currState][currAction][currDestState];
    			utilitySum += mdp.transProb[currState][currAction][currDestState] * 
    					(mdp.reward[currState]+discount*policyEvalUtility[currDestStateIndex]);

    		}
    		if(utilitySum > bestUtility)
    		{
    			bestUtility = utilitySum;
    			bestAction=currAction;
    		}
    	}
    	return bestAction;
    }
  
}
