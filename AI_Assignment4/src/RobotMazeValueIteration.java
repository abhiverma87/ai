import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This is the template of a class that should run value iteration on a given
 * MDP to compute the optimal policy which is returned in the public
 * <tt>policy</tt> field. The computed optimal utility is also returned in the
 * public <tt>utility</tt> field. You need to fill in the constructor. You may
 * wish to add other fields with other useful information that you want this
 * class to return (for instance, number of iterations before convergence). You
 * also may add other constructors or methods, provided that these are in
 * addition to the one given below (which is the one that will be automatically
 * tested). In particular, your code must work properly when run with the
 * <tt>main</tt> provided in <tt>RunCatMouse.java</tt>.
 */
public class RobotMazeValueIteration {

	/** the computed optimal policy for the given MDP **/
	public int policy[];

	/** the computed optimal utility for the given MDP **/
	public double utility[];

	/**
	 * The constructor for this class. Computes the optimal policy for the given
	 * <tt>mdp</tt> with given <tt>discount</tt> factor, and stores the answer
	 * in <tt>policy</tt>. Also stores the optimal utility in <tt>utility</tt>.
	 */
	public RobotMazeValueIteration(Mdp mdp, double discount) {

		// your code here
		System.out.println("Num States : " + mdp.numStates);
		System.out.println("Num Actions : " + mdp.numActions);
		System.out.println("Discount : " + discount);
		int numStates = mdp.numStates;
		this.utility = new double[numStates];
		// Initializing the policy array
		this.policy = new int[numStates];
		double delta = Math.pow(10, -8) * (1 - discount) / discount; /*
																	 * TODO find
																	 * out about
																	 * error
																	 * factor
																	 */
		double difference;
		double maxDifference;
		double tempUtility[] = new double[numStates];
		boolean continueLoop = true;
		List<Integer> unchangedStates = new ArrayList<Integer>();
		unchangedStates.add(6);
		unchangedStates.add(10);
		Scanner scan = new Scanner(System.in);
		System.out.println("Running Value Iteration : ");
		int count=0;
		PrintGrid pr=new PrintGrid();
		double maxUtil;
		double stateUtil;
		int destinationState;
		int optimalAction;
		int numActions = mdp.numActions;
		while (continueLoop) {
			System.out.println("Iteration No. "+count+" : ");
			count++;
			maxDifference = Double.NEGATIVE_INFINITY;
			for (int i = 0; i < numStates; i++) {
				if (unchangedStates.contains(i)) {
					if (i == 6) {
						tempUtility[i] = -1;
					} else if (i == 10) {
						tempUtility[i] = 1;
					}
				}
				this.utility[i] = tempUtility[i];
				//System.out.print(this.utility[i]+" ");
			}
			pr.printGrid(3, 4, tempUtility);
			System.out.println("Press return key to continue");
			scan.nextLine();
			//System.out.println();
			// Updating the grid space for optimal policies
			for (int i = 0; i < numStates; i++) {
				optimalAction = -1;
				maxUtil = Double.NEGATIVE_INFINITY;
				for (int j = 0; j < numActions; j++) {
					stateUtil = 0.0;
					for (int k = 0; k < mdp.nextState[i][j].length; k++) {
						destinationState = mdp.nextState[i][j][k];
						stateUtil += (mdp.transProb[i][j][k])
								* (this.utility[destinationState]);
					}
					/*System.out.print(this.direction_map.get(i) + "," + stateUtil
							+ "   ");*/
					if (stateUtil > maxUtil) {
						maxUtil = stateUtil;
						optimalAction = j;
					}
				}
				this.policy[i] = optimalAction;
				tempUtility[i] = mdp.reward[i] + (discount)
						* maxUtil;
				difference = Math.abs(this.utility[i] - tempUtility[i]);
				if (!unchangedStates.contains(i) && difference > maxDifference) {
					maxDifference = difference;
				}
			}
			
			/*for (int i = 0; i < numStates; i++) {
				System.out.print(tempUtility[i]+" ");
			}
			System.out.println();*/
			if (delta > maxDifference) {
				continueLoop = false;
			}
		}
	}
}
